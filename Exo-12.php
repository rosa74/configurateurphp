<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>    

    <?php

    // Avec la fonction array-push, ajoutez un élément à la fin d'un tableau.
    // Faites un tableau indéxé avec les pièces de votre maison. Ajoutez la cuisine avec array_push.
    // Affichez la liste des pièces de votre maison
    
    ?>
    
    <!-- écrire le code après ce commentaire -->
    <?php

    

    $pieces =["chambre","salon","salle de bain","wc"];

    array_push($pieces,"cuisine");

    foreach ($pieces as $cuisine){
        echo $cuisine .'<br>';
    }
    ?>
    <!-- écrire le code avant ce commentaire -->

</body>
</html>